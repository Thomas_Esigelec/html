<?php session_start()?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Page des trajets</title>
		<link rel="stylesheet" href="style.css">
		
	</head> 
	
	<body>
		<header>Page des trajets</header>

		<nav>
		Rugby covoit'
		</nav>
	
		<aside class=contenant>
			<div id="Menu">Menu principal</div>
			<div class=choix><a href="acceuil.php"> Accueil </a></div>
			<div class=choix><a href="connexion.php"> Connexion </a></div>
			<div class=choix><a href="Inscription.php">Inscription</a></div>
			<div class=choix><a href="trajets.php">Afficher les trajets</a></div>
		</aside>
		
		
		<section class=contenant>
			<div id="Bienvenue"> Voici la liste des trajets</div>
			<?php
			//$mysqli = new mysqli("localhost", "root", "root", "bddgr1310");
            $mysqli = new mysqli("MODULEWEB.intranet.int", "gr1310JXoi", "CN2a69NB", "bddgr1310");
			
			if ($mysqli->connect_errno) {
				echo "Erreur lors de la connexion";
			} else {
				$results = $mysqli->query("SELECT * FROM trajet");
				if($results){
					echo "<table class='tra'>
							<tr>
								<th>Date</th>
								<th>Départ</th>
								<th>Arrivée</th>
								<th>Prix</th>
								<th>Nombre de place(s)</th>
							</tr>" ;
					while($ligne = $results->fetch_assoc()){
						echo '
						<tr>
							<td>'.$ligne['datedepart'].'</td>
							<td>'.$ligne['villedepart'].'</td>
							<td>'.$ligne['villearrivee'].'</td>
							<td>'.$ligne['prix'].' €</td>
							<td>'.$ligne['nbdeplace'].'</td>
						</tr>' ;
					}
					
					echo "</table>" ;
				}
			}
			?>
			
		</section>
		
		
		<footer>
			Copyright ESIGELEC 2015
		</footer>
		
	</body>
</html>