-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 05 Novembre 2015 à 22:44
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `bddgr1310`
--

-- --------------------------------------------------------

--
-- Structure de la table `membre`
--

CREATE TABLE `membre` (
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `idmembre` int(11) NOT NULL,
  `telephone` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `trajet`
--

CREATE TABLE `trajet` (
  `idtrajet` int(11) NOT NULL,
  `nbdeplace` int(11) NOT NULL,
  `villedepart` varchar(50) NOT NULL,
  `villearrivee` varchar(50) NOT NULL,
  `prix` decimal(5,2) NOT NULL,
  `datedepart` date NOT NULL,
  `idtrajetmatch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `trajet`
--

INSERT INTO `trajet` (`idtrajet`, `nbdeplace`, `villedepart`, `villearrivee`, `prix`, `datedepart`, `idtrajetmatch`) VALUES
(1, 4, 'Rouen', 'Londres', '50.00', '2015-11-01', 18),
(2, 2, 'Paris', 'NewCastle', '87.00', '2015-10-21', 123),
(3, 1, 'Marseille', 'Cardiff', '120.00', '2015-11-02', 12763),
(4, 6, 'Paris', 'Birmingham', '72.00', '2015-10-26', 15263);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `membre`
--
ALTER TABLE `membre`
  ADD PRIMARY KEY (`idmembre`);

--
-- Index pour la table `trajet`
--
ALTER TABLE `trajet`
  ADD PRIMARY KEY (`idtrajet`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `membre`
--
ALTER TABLE `membre`
  MODIFY `idmembre` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `trajet`
--
ALTER TABLE `trajet`
  MODIFY `idtrajet` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;